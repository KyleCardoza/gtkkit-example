#import "MainWindow.h"

@implementation MainWindow

- (id)init {
	if (self = [super init]) {
	
		self.title = @"Hello!";
		self.borderWidth = 10;
		self.resizable = false;
		self.windowDestroyedCallback = ^ {
		  of_log(@"Goodbye, cruel world!");
		  gtk_main_quit();
		};
		self.onDestroy = self.windowDestroyedCallback;

		self.frame = [GTKFrame new];
		self.frame.label = @"Hello, world!";

		self.grid = [GTKGrid new];
		self.grid.orientation = [Orientation vertical];
		self.grid.columnsHomogeneous = true;
		self.grid.borderWidth = 5;

		self.entry = [GTKEntry new];
		self.entry.textVisible = false;
		self.entry.maximumLength = 16;

		self.button = [GTKButton new];
		self.button.label = @"Click Me!";
		self.button.borderWidth = 5;
		
		self.buttonDelegate = [ExampleDelegate new];
		self.button.delegate = self.buttonDelegate;

		[self addWidget: self.frame];
		[self.frame addWidget: self.grid];
		[self.grid addWidget: self.entry];
		[self.grid addWidget: self.button];
	}
	return self;
}

@end
