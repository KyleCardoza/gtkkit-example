#import <ObjFW/ObjFW.h>
#import <gtk/gtk.h>
#import <GTKKit/GTKKit.h>
#import "ExampleDelegate.h"

@class ExampleDelegate;

@interface MainWindow : GTKWindow

@property (nonnull) GTKGrid *grid;
@property (nonnull) GTKFrame *frame;
@property (nonnull) GTKButton *button;
@property (nonnull) GTKEntry *entry;
@property (assign, nullable) GTKCallback windowDestroyedCallback;
@property (assign, nullable) GTKCallback buttonCallback;
@property (nullable) ExampleDelegate *buttonDelegate;

@end
