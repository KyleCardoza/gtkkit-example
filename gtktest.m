#import <ObjFW/ObjFW.h>
#import <gtk/gtk.h>
#import <GTKKit/GTKKit.h>
#import "MainWindow.h"


int main (int argc, char *argv[]) {
	@autoreleasepool {
		gtk_init (&argc, &argv);

		MainWindow *win = [MainWindow new];
		
		[win showAll];
		
		__unsafe_unretained typeof(win) wwin = win;
		win.buttonCallback = ^(GTKButton *sender){
			of_log(wwin.entry.stringValue);
		};
		win.button.onClick = win.buttonCallback;

		gtk_main();
	}
	return 0;
}
